#include <zpj/zpj-goa-authorizer.h>
#include <zpj/zpj-drive.h>
#include <zpj/zpj-enums.h>

#include <stdio.h>

static void
test_zpj (GoaObject *goa_account)
{
  ZpjGoaAuthorizer *auth = NULL;
  ZpjDrive *drive = NULL;
  ZpjDriveItem *root = NULL;
  GError *error = NULL;
  GList *list = NULL, *list_iterator = NULL;
  gchar *drive_id = NULL;
  ZpjDriveType drive_type = ZPJ_DRIVE_TYPE_PERSONAL;
  
  auth = zpj_goa_authorizer_new (goa_account);
  
  list = zpj_drive_get_drives (ZPJ_AUTHORIZER (auth), NULL, &error);
  if (error != NULL)
    {
      printf ("Encountered error: %s\n", error->message);
      g_clear_error (&error);
      goto end;
    }
  
  printf ("\nListing drives: \n");
  for (list_iterator = list; list_iterator != NULL; list_iterator = g_list_next (list_iterator))
    {
      drive = list_iterator->data;
      
      g_object_get (drive, 
                    "id", &drive_id,
                    "type", &drive_type,
                    NULL);
      printf (" - drive: %s (%s)\n", drive_id, g_enum_to_string (ZPJ_TYPE_DRIVE_TYPE, drive_type));
      
      g_free (drive_id);
      drive_id = NULL;
      g_clear_object (&drive);
    }
  printf ("Done!\n\n");
  
  drive = zpj_drive_get_default (ZPJ_AUTHORIZER (auth), NULL, &error);
  
  g_object_get (drive, 
                "id", &drive_id,
                "type", &drive_type,
                NULL);
  printf ("Default drive: %s (%s)\n", drive_id, g_enum_to_string (ZPJ_TYPE_DRIVE_TYPE, drive_type));

  printf ("Getting root of the default drive: ");
  root = zpj_drive_get_root (drive, NULL, &error);
  if (!root)
    {
      printf ("error: %s\n\n", error->message);
      g_clear_error (&error);
    }
  else
    {
      printf ("%s of name %s\n\n", zpj_drive_item_is_file (root) ? "file" : "folder", zpj_drive_item_get_name (root));
    }
  
end:
  g_free (drive_id);
  g_clear_object (&drive);
  g_list_free (list);
}

int
main (int    argc,
      char **argv)
{
  GoaClient *client = NULL;
  GList *accounts = NULL;
  GList *account_iterator = NULL;
  GoaObject *object = NULL;
  GoaAccount *account = NULL;
  GError *error = NULL;
  gchar *provider_type = NULL;
  gchar *provider_name = NULL;
  gchar *identity = NULL;
  
  client = goa_client_new_sync (NULL, &error);
  if (error != NULL)
    {
      fprintf (stderr, "Error creating client\n");
      return 1;
    }
  
  accounts = goa_client_get_accounts (client);
  
  for (account_iterator = accounts; account_iterator != NULL; account_iterator = account_iterator->next)
    {
      object = GOA_OBJECT (account_iterator->data);
      account = goa_object_get_account (object);
      
      g_object_get (G_OBJECT (account),
                    "provider-type", &provider_type,
                    "provider-name", &provider_name,
                    "presentation-identity", &identity, NULL);
      
      printf ("Got an account: %s (%s)", identity, provider_name);
      
      if (g_strcmp0 (provider_type, "ms_graph") == 0)
        {
          printf (" - matches: testing\n");
          test_zpj (object);
        }
      else
        {
          printf ("\n");
        }
      
      g_free (provider_type);
      g_free (provider_name);
      g_free (identity);
      g_object_unref (account);
      g_object_unref (object);
    }
  
  g_list_free (accounts);
  
  g_object_unref (client);
  
  return 0;
}

